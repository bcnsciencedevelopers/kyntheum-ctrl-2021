var menuPrincipal;
var estructuraMenu;
var estructura;

function menu() {

    estructura = [];
    estructura[0] = ["ES_KYNTHEUM_2021_C2_CTRL_00",  [
        "ES_KYNTHEUM_2021_C2_CTRL_00_00", // Home
        "ES_KYNTHEUM_2021_C2_CTRL_00_01", // ADHERENCIA
        "ES_KYNTHEUM_2021_C2_CTRL_00_02", // ADHERENCIA
        "ES_KYNTHEUM_2021_C2_CTRL_00_03", // ADHERENCIA
        "ES_KYNTHEUM_2021_C2_CTRL_00_04", // ADHERENCIA
        "ES_KYNTHEUM_2021_C2_CTRL_00_05", // PSP
        "ES_KYNTHEUM_2021_C2_CTRL_00_06", // PSP
        "ES_KYNTHEUM_2021_C2_CTRL_00_07", // PSP
        "ES_KYNTHEUM_2021_C2_CTRL_00_08", // PSP
        "ES_KYNTHEUM_2021_C2_CTRL_00_09", // PACK DE INICIO
        "ES_KYNTHEUM_2021_C2_CTRL_00_10", // PACK DE INICIO
        "ES_KYNTHEUM_2021_C2_CTRL_00_11", // PACK DE INICIO
        "ES_KYNTHEUM_2021_C2_CTRL_00_12", // CTRL
        
    ]];

    estructura[99] = ["ES_KYNTHEUM_2021_C2_CTRL_FT",  [
        "ES_KYNTHEUM_2021_C2_CTRL_FT_00"
    ]];

    estructuraMenu0 = [
        {
            "goto": {
                "slide": "1",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "goto": {
                "slide": "5",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "goto": {
                "slide": "9",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "goto": {
                "slide": "12",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }
    ];
    estructuraMenu1 = [
        {
            "goto": {
                "slide": "1",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "goto": {
                "slide": "5",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "goto": {
                "slide": "9",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }, {
            "goto": {
                "slide": "12",
                "presentation": "0",
                "touch": true
            },
            "children": false
        }
    ];

    menuPrincipal0 = new Menu(estructuraMenu0, "mainMenu0");
    menuPrincipal1 = new Menu(estructuraMenu1, "menuLateral");

    if (location.host === "localhost") {
        edetailing.enableTestMode();
    }
    inicializar_navigate();
}