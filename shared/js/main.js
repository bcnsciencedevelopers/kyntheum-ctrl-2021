var materials = [];
var buttonsSelected = [];

$(document).ready(function () {
    // logo palexia ficha tecnica
    // $("#global").append("<div class='logo-marca js-goto-touch' data-presentation='99' data-slide='0'></div>");
    menu();
    espec();
    main();
});

function main() {

    // logo
    // $('#global').append('<div class="logo-farma"></div>');

    // menu
    $('#global').append('<div id="mainMenu0"></div>');
    $('#global').append('<div id="menuLateral"></div>');


}

/****************
 FUNCIONES
 ****************/
function openPop(id) {
    if ($('.btn-referencias').hasClass('disabled')) {
        return;
    }
    $("#" + id + ", " + "#imgPopGloss").css('display', "block").css('z-index', 9999);
    $("#" + id).animate({ opacity: 1 }, edetailing.popupActual.dataTemps, function () { });
    $("#overlay").addClass('active');

}

function closePop(id) {
    $("#" + id).css('z-index', -1);
    $("#" + id).animate({ opacity: 0 }, edetailing.popupActual.dataTemps, function () {
        $("#" + id).css('display', "none");
    });
    $("#overlay").removeClass('active');
    if (id === 'popAE') {
        $('#line').html('');
    }
}


/**
 * Cuando clicas una parte del cuerpo, se cambia la img
 */
$('.img').on('click', function () {
    var id = $(this).attr('id');
    var lastChar = id[id.length - 1];
    var penultimateChar = id[id.length - 2];

    // Popup
    $("#" + id).attr('onclick', 'openPop(\"popBig\")');
    $("#overlay").addClass('active');
    $("#overlay").css('z-index', '99');

    if ($.isNumeric(penultimateChar)) {
        $("#global").append("<img src='css/img/casos_clinicos/1/img-" + penultimateChar + lastChar + "-big.png' class='imgpopupBig' id='popBig'>");
    } else {
        $("#global").append("<img src='css/img/casos_clinicos/1/img-" + lastChar + "-big.png' class='imgpopupBig' id='popBig'>");
    }


    $('#popBig').on('click', function () {
        $("#overlay").css('z-index', '-199');
        $("#overlay").removeClass('active');
        $("#popBig").remove();
    });
});